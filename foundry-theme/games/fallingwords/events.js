const events = (() => {
    let onKey = null;


    document.addEventListener('keypress', e => {
        if(onKey != null)
            onKey(e.key);
    });

    return {
        set onKey(callBack){
            onKey = callBack;
        }
    }
})();
export default events;
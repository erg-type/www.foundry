const videoElement = document.getElementById('video');
const canvasElement = document.getElementById('canvas');
const context = canvasElement.getContext('2d');

videoElement.style.display = 'none';

const minOpacity = 0.05; 
const maxOpacity = 1;
const brightnessThreshold = 100;

async function initializeCamera() {
    try {
        const mediaStream = await navigator.mediaDevices.getUserMedia({ video: true });
        videoElement.srcObject = mediaStream;
    } catch (error) {
        console.error("Erreur d'accès à la caméra : ", error);
    }
}

function computeBrightness() {
    context.drawImage(videoElement, 0, 0, canvasElement.width, canvasElement.height);
    const imageData = context.getImageData(0, 0, canvasElement.width, canvasElement.height);
    const pixelData = imageData.data;

    let totalBrightness = 0;
    const totalPixels = pixelData.length / 4;

    for (let i = 0; i < pixelData.length; i += 4) {
        totalBrightness += 0.2126 * pixelData[i] + 0.7152 * pixelData[i + 1] + 0.0722 * pixelData[i + 2];
    }

    const averageBrightness = totalBrightness / totalPixels;
    adjustTextOpacity(averageBrightness);
}

function adjustTextOpacity(brightness) {
    let newOpacity;

    if (brightness < brightnessThreshold) {
        newOpacity = minOpacity; 
    } else {
        newOpacity = minOpacity + ((brightness - brightnessThreshold) / (255 - brightnessThreshold)) * (maxOpacity - minOpacity);
        newOpacity = Math.min(maxOpacity, newOpacity); 
    }

    document.body.style.opacity = newOpacity;

    const brightnessFactor = brightness < brightnessThreshold ? 0.5 : 1 + (brightness - brightnessThreshold) / (255 - brightnessThreshold);
    context.filter = `brightness(${brightnessFactor})`;
    context.drawImage(videoElement, 0, 0, canvasElement.width, canvasElement.height);
}

let lastFrameTime = 0;
const frameInterval = 50; 

function refresh(timestamp) {
    if (timestamp - lastFrameTime >= frameInterval) {
        if (videoElement.readyState === videoElement.HAVE_ENOUGH_DATA) {
            if (canvasElement.width !== videoElement.videoWidth || canvasElement.height !== videoElement.videoHeight) {
                canvasElement.width = videoElement.videoWidth;
                canvasElement.height = videoElement.videoHeight;
            }
            computeBrightness();
            lastFrameTime = timestamp;
        }
    }
    requestAnimationFrame(refresh);
}

initializeCamera();
videoElement.addEventListener('loadeddata', () => {
    requestAnimationFrame(refresh);
});

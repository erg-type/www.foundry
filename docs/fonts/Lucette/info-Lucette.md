---
title: Lucette
date_pub: 2022/27/01, 14:07:42
font_family: Lucette
project_url: https://yannlinguinou.com/TYPEFACE
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Yann Linguinou, https://yannlinguinou.com]
styles:
 - Regular
 - Regular italic
paths:
 - fonts/Lucette/Lucette-Regular.otf
 - fonts/Lucette/Lucette-Regularitalic.otf
tags:
 - Serif
ascender: 980
descender: -220
sample_text: "Tout ça n’est qu’une grosse fiction, et c’est une super bonne nouvelle."
public: yes
---

 

---
title: Lunar
date_pub: 2022/27/01, 16:04:32
font_family: Lunar
project_url: https://lilojoris.com/typography
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Lilo Joris]
styles:
 - Regular
 - Stencil
paths:
 - fonts/Lunar/Lunar.otf
 - fonts/Lunar/Lunar-Stencil.otf
tags:
 - Serif
ascender: 800
descender: -255
sample_text: "L’université c’est pour rencontrer des gens à jeun.
"
public: yes
---

 

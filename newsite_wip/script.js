const trigger = document.getElementById("home-button");
const dropdown = document.querySelector("#home-menu menu");
let font = "KraftMono";
let extension = "otf";
//
// trigger.addEventListener("click", () => {
//   dropdown.classList.toggle("visible");
// });
//
window.onload = function () {
  document.getElementById("inneriframe").focus();
};
/*script menu dropdown*/
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("dropdownShow");
}

/* fonction très bg -> va récuperer le nom de la police et l'extension de la police sur lequel on a cliqué
 et va mettre cette fonte dans une variable css
  et va trouver tous les glyphes supportés par cette font et va les mettre dans la grille en bas*/
const fontbuttons = document.querySelectorAll(".font");
fontbuttons.forEach((button) => {
  //set the font of the button to the font-name attribute
  button.style.fontFamily = button.getAttribute("font-name");
  button.addEventListener("click", () => {
    font = button.getAttribute("font-name");
    extension = button.getAttribute("font-extension");
    if (font === null) {
      font = "KraftMono";
      extension = "otf";
    }
    document.documentElement.style.setProperty("--font", font);
    console.log(font, extension);

    fetch(`./fonts/${font}.${extension}`)
      .then((res) => res.arrayBuffer())
      .then((data) => {
        let fontjs = opentype.parse(data);
        let container = document.getElementById("glyphContainer");
        container.innerHTML = "";
        for (let i = 0; i < fontjs.glyphs.length; i++) {
          let glyph = fontjs.glyphs.get(i);
          if (glyph.unicode !== undefined) {
            let span = document.createElement("span");
            span.style.fontFamily = font;
            span.textContent = String.fromCharCode(glyph.unicode);
            span.style.margin = "0 5px";
            container.appendChild(span);
          }
        }
      })
      .catch((err) => console.error("Error loading font:", err));
  });
});

// Close the dropdown menu if the user clicks outside of it
// doesnt work right now I don't think...
window.onclick = function (event) {
  if (!event.target.matches(".dropbtn")) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("show")) {
        openDropdown.classList.remove("show");
      }
    }
  }
};

// Boutons pour changer les couleurs

function changeColor1() {
  const colorPicker = document.getElementById("colorPicker1");
  let selectedColor = colorPicker.value;
  console.log(selectedColor);
  document.documentElement.style.setProperty("--color1", selectedColor);
}

function changeColor2() {
  const colorPicker = document.getElementById("colorPicker2");
  let selectedColor = colorPicker.value;
  console.log(selectedColor);
  document.documentElement.style.setProperty("--color2", selectedColor);
}

function changeColor3() {
  const colorPicker = document.getElementById("colorPicker3");
  let selectedColor = colorPicker.value;
  console.log(selectedColor);
  document.documentElement.style.setProperty("--color3", selectedColor);
}
// Boutons pour changer le corps de textearea

function changeFontSize() {
  const field = document.getElementById("textarea"),
    showValue = document.getElementById("fontSizeValue"),
    picker = document.getElementById("fontSizeSlider");
  let selectedSize = picker.value;
  console.log(selectedSize);
  showValue.innerHTML = selectedSize + " pt";
  field.style.fontSize = selectedSize + "pt";
  field.style.lineHeight = selectedSize + "pt";
}

// TODO: changer la police de tout le font-info
// ça fait des erreurs le truc en dessous là
//
// document
//   .getElementsByClassName("unsetAll")
//   .addEventListener("click", function () {
//     field = document.getElementById("font-info");
//     let selectedFont = document.getElementsByClassName("font").fontFamily;
//     console.log(selectedFont);
//     field.style.fontFamily = selectedFont;
//   });

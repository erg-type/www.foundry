---
title: espatarra-08.4
date_pub: 2022/27/01, 16:13:02
font_family: espatarra-08.4
project_url: https://yannlinguinou.com/TYPEFACE
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Yann Linguinou]
styles:
 - Regular
paths:
 - fonts/Espatarra/Espatarra-Regular.otf
tags:
 - Serif
ascender: 1000
descender: -200
sample_text: "Ma grand-mère qui disait « oui » à tout le pronçait « vi » (vie), elle pleurait aussi.
"
public: yes
---

 

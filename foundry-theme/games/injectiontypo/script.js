//TODO: recevoir le chemin d'une typo à utiliser depuis la page du site
//(ça peut être via un # renseigné par la iframe)
//en attendant voici:
const fontsPath = '../../docs/fonts/';
const fonts = ['BXlineMAC/BXlinePC-Normal.otf', 'Caslongue/Caslongue-Bodybolter-Regular.otf', 'Caslongue/Caslongue-Bosobiso.otf', 'Cosmonette/CosmonetteTrial-Regular.otf', 'Futturra/futturra.otf', 'Opbouw/opbouw_regular.otf'];

//un script qui télécharge une page html, l'insère dans le dom courant et applique une typo
const request = async(requestUrl) => {
        
    const response = await fetch('pages/'+requestUrl);
    const content = await response.text();
    return content;
    
}

document.querySelector('head').innerHTML += `
    <style>
    @font-face {
        font-family: typo;
        src: url(${fontsPath}${fonts[Math.floor(Math.random()*fonts.length)]});
    }
    html, body{
        font-family: typo;
        margin:0;
        padding:0;
        
    }
   
    </style>
`;

const pages = ['mfwebsite.html', 'perdu.html', 'web.html'];

request(pages[Math.floor(Math.random() * pages.length)]).then(html => {
    document.querySelector('main').innerHTML = html;
});
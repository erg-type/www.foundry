import word from "./word.js"
import events from "./events.js"


//TODO: recevoir le chemin d'une typo à utiliser depuis la page du site
//(ça peut être via un # renseigné par la iframe)
//en attendant voici:
const fontsPath = '../../docs/fonts/';
const fonts = ['BXlineMAC/BXlinePC-Normal.otf', 'Caslongue/Caslongue-Bodybolter-Regular.otf', 'Caslongue/Caslongue-Bosobiso.otf', 'Cosmonette/CosmonetteTrial-Regular.otf', 'Futturra/futturra.otf', 'Opbouw/opbouw_regular.otf'];

document.querySelector('head').innerHTML += `
    <style>
    @font-face {
        font-family: typo;
        src: url(${fontsPath}${fonts[Math.floor(Math.random()*fonts.length)]});
    }
    html, body{
        font-family: typo;
    }

    </style>
`;

const txt = [
    'saperlipopette', 
    'sapristi', 
    'palsembleu', 
    'perlimpimpin', 
    'abracadabra', 
    'tourniquet'
];
const $scene = document.querySelector('main');



let creationInterval = 4000;
let speedUpInterval = 8000;
let nbWordsPerGen = 3;


let words = [], lastSpeedUp, lastGeneration, minSpeed, maxSpeed;

const reset = () => {
    words.forEach(word => {
        word.destroy();
    });
    words = [];
    lastSpeedUp = Date.now();
    lastGeneration = Date.now();
    minSpeed = 0.1;
    maxSpeed = 0.3;
}



const checkWords = (key) => {
    words.forEach(word => {
        word.checkLetter(key);
    });
};


const loop = () => {
    //console.log(minSpeed + Math.random() * (maxSpeed - minSpeed));
    if(Date.now() - lastSpeedUp > speedUpInterval){
        minSpeed += 0.1;
        maxSpeed *= maxSpeed;
        lastSpeedUp = Date.now();
    }
    //console.log(Date.now() - lastGeneration);
    if(Date.now() - lastGeneration > creationInterval || words.length == 0){
        for(let i = 0; i < nbWordsPerGen; i++){
            words.push(word(
                $scene, txt[Math.floor(Math.random() * txt.length)],
                minSpeed + Math.random() * (maxSpeed - minSpeed) 
            ));

        }
        lastGeneration = Date.now();
    }
    //console.log(words);
    words.forEach((word, index, list) => {
        word.update();
        if(word.pos.y > $scene.offsetHeight){
            reset();
        }
        if(word.disabled){
            word.destroy();
            list.splice(index, 1);
        }

    });
    requestAnimationFrame(loop);
};

reset();

requestAnimationFrame(loop);

events.onKey = checkWords;


---
title: Untitled1
date_pub: 2022/27/01, 14:34:13
font_family: Futturra
project_url: 
license: CC BY-ND
license_url: https://creativecommons.org/
designers:
 - [Elias Sanhaji]
styles:
 - Regular
paths:
 - fonts/Futturra/futturra.otf
tags:
 - Sans Serif
ascender: 800
descender: -200
sample_text: "Les parents ne sont plus modulables, leurs principes sont comme du ciment. Pire, ils sont comme du riz gluant qui a séché et qui est resté collé dans un bol.
"
public: yes
---

 

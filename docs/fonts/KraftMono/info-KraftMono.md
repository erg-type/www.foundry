---
title: Kraft Mono
date_pub: 2022/27/01, 12:04:33
font_family: Kraft Mono
project_url: www.exemple.com
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Renaud Guiliano,]
styles:
 - Regular
 - Italic
paths:
 - fonts/KraftMono/KraftMono.otf
 - fonts/KraftMono/KraftMono-Italic.otf
tags:
 - Monospace
 - Serif
ascender: 750
descender: -250
sample_text: "Récite moi l'alphabet."
public: no
---

 

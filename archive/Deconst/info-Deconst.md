---
title: Deconst
date_pub: 2022/25/01, 12:11:44
font_family: Deconst
project_url: 
license: 
license_url: 
designers:
 - [, ]
styles:
 - extend
paths:
 - fonts/Deconst/Deconst.otf
tags:
ascender: 1000
descender: -200
sample_text: "Récite moi l'alphabet."
public: no
---

 
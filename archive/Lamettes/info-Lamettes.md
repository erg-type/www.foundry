---
title: Lamettes
date_pub: 2022/25/01, 12:11:44
font_family: Lamettes
project_url: 
license: 
license_url: 
designers:
 - [, ]
styles:
 - Bold
paths:
 - fonts/Lamettes/LamettesBold.otf
tags:
ascender: 800
descender: -200
sample_text: "Récite moi l'alphabet."
public: no
---

 
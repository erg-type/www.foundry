const menu = document.getElementById("sidebar"),
      menubutton = document.getElementById("collapse-button");

let $menuWidth,
    $rightSideWidth;

menubutton.innerHTML = '> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >';

menubutton.addEventListener("click", function () {
  sidebarCollapse();
})

window.onload = function() {
  $menuWidth = menu.offsetWidth + 4;
  $rightSideWidth = $menuWidth;
}

window.addEventListener('resize', function(event){
  $menuWidth = menu.offsetWidth + 4;
  $rightSideWidth = $menuWidth;
});

function sidebarCollapse() {
  if (menu.style.marginRight == `-${$rightSideWidth}px` || menu.style == '') {
    menu.style.marginRight = '0px';
menubutton.innerHTML = '> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >';
}
  else{
  $menuWidth = menu.offsetWidth + 4;
  $rightSideWidth = $menuWidth;
    menu.style.marginRight = `-${$rightSideWidth}px`;
menubutton.innerHTML = '< < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < <';
  }

}

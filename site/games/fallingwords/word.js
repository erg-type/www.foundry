const word = ($scene, text, speedY) => {
    const $element = document.createElement('div');
    const pos = {'x':0, 'y':-20};
    const speed = {'x':0, 'y':speedY};
    const $letters = [];
    let letterInd = 0;
    let disabled = false;
    const init = () => {
        $element.classList.add('word', 'active');
        text.split('').forEach(letter => {
            const $letter = document.createElement('span');
            $letter.innerText = letter;
            $element.append($letter);
            $letters.push($letter);
        });
        
        $scene.append($element);
        //console.log($scene.innerWidth);
        pos.x = Math.random() * ($scene.offsetWidth - $element.offsetWidth);
        

    };
    const checkLetter = (key) => {
        if(key == text[letterInd]){
            $letters[letterInd].classList.add('validate');
            letterInd++;
            if(letterInd >= $letters.length){
                disabled = true;
            }
        }else{
            $letters.forEach($letter => {
                $letter.classList.remove('validate');
            });
            letterInd = 0;
        }
    };

    const update = () => {
        //console.log(speed.y);
        pos.x += speed.x;
        pos.y += speed.y;
        
        $element.style.left = `${pos.x}px`;
        $element.style.top = `${pos.y}px`;


    };

    const destroy = () => {
        $element.remove();
    };
    
    
    
    
    init();

    return {update, checkLetter, destroy, 
        get disabled(){return disabled}, get pos(){return pos}
    }

};

export default word;
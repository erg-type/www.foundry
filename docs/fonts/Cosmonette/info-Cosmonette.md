---
title: DIANA_REGULAR
date_pub: 2022/27/01, 14:44:33
font_family: Cosmonette
project_url: https://maudserradell.com/
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Maud Serradell]
styles:
 - Regular
paths:
 - fonts/Cosmonette/CosmonetteTrial-Regular.otf
tags:
 - Sans Serif
ascender: 1033
descender: -167
sample_text: "Pour fabriquer un ordinateur il faut environ 700 migrants."
public: yes
---

 

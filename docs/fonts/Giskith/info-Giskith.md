---
title: Giskith
date_pub: 2022/27/01, 14:53:06
font_family: Giskith
project_url: 
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Clement Baudouin]
styles:
 - Regular
paths:
 - fonts/Giskith/Giskith.otf
tags:
 - Serif
ascender: 750
descender: -250
sample_text: "C’est ma petite sœur qui m’a dit que Saint Nicolas n’existe pas.
"
public: yes
---

 

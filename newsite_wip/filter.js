// Pour faire descendre comme il faut le menu déroulant

const filterButton = document.getElementById('filter-dropdown-button'),
      filterDropdown = document.getElementById('filtersContainer');

filterButton.addEventListener('click', () => {
    filterDropdown.classList.toggle('show');
});

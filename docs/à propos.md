---
title: à propos
date_pub: 
public: yes
---
#Chasse ouverte! KEZAKO?

Vous êtes ici sur le site de la typothèque de l'erg, collection de fontes crées par les étudiant·es du cours de typographie.

#Historique
## Rip l'ancienne typothèque
Il existait précédement une [typothèque](https://copyright.rip/erg/typo/) de l'erg basée sur [Ofont](https://gitlab.com/raphaelbastide/usemodify), un système opensource pour cataloguer les fontes en ligne.

## une première mouture
En 2022 une première recherche afin de concevoir une nouvelle typothèque qui pourrait cataloguer les fontes, publier des articles, des interviews, montrer des exemples de fontes utilisées a été initiée. Laissée en jachère après un premier workshop, il s'agissait des premiers échaffaudages.

# Ici et maintenant
Ce site a été réalisé dans le cadre d'un workshop en novembre 2024 initié par Ludi Loiseau et Antoine Gelgon en spécial guest.
A partir d'un reboot du dessin de l'interface, sur papier et sur écran nous nous sommes chauffé sur nos clavier en implémentant les fichiers html et css. Une charpente commune a été pensée pour la structure du site et les templates de pages. Ce fut l'occasion de découvrir MkDocs un générateur de site static super léger, de créer les contenus en markdown, d'écrire les templates en yaml, et de créer des jeux de lecture en Java script. 
Il ne restait plus qu'à rassembler et curater les fontes à publier.

Participant·es: 
Alexia de Visscher
Antoine Gelgon
Delyo Dobrev
Elliot Thomas
Feuf Vivant
Jeremy Agboton
Lionel Maes
Louka Langenbick
Ludi Loiseau 
Manon Compagnon
Sable Drique
Teilo Jaubert
Titouan Le Bellegard 
Vincent Brun

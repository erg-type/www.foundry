---
title: Caslongue
date_pub: 2022/25/01, 12:11:44
font_family: Caslongue
project_url: 
license: 
license_url: 
designers:
 - [ERG, https://wiki.erg.be/m/]
styles:
 - Elasticgirls
 - Metacursive
 - Bosobiso
 - Regular
 - Metaveste
 - Fourline
paths:
 - fonts/Caslongue/Caslongue-Elasticgirls.otf
 - fonts/Caslongue/Caslongue-Metacursive.otf
 - fonts/Caslongue/Caslongue-Bosobiso.otf
 - fonts/Caslongue/Caslongue-Bodybolter-Regular.otf
 - fonts/Caslongue/Caslongue-Metaveste.otf
 - fonts/Caslongue/Caslongue-Fourline.otf
tags:
 - Serif
ascender: 1600
descender: -400
sample_text: "POUR FABRIQUER UN ORDINATEUR IL FAUT ENVIRON 700 MIGRANTS."
public: yes 
---

 

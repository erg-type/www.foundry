---
title: Lucette
date_pub: 2022/25/01, 12:11:44
font_family: Lucette
project_url: 
license: 
license_url: 
designers:
 - [, ]
styles:
 - Regular
 - Regular italic
paths:
 - fonts/Lucette/Lucette-Regular.otf
 - fonts/Lucette/Lucette-Regularitalic.otf
tags:
ascender: 980
descender: -220
sample_text: "Récite moi l'alphabet."
public: no
---

 
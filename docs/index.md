---
title: home 
---
## FreeFontLibrary

This is a demo version of FreeFontLibrary.

## Credits

This site uses Free Font Library, a tool to manage and display a font collection on the web.

- *Design & Development*: [Luuse](luuse/io)
- *Download*: [Gitlab](gitlab.com/Luuse/Luuse.tools/mkdocks-typotheque)
- *License*: [GNU/GPL](https://www.gnu.org/licenses/gpl.html)
- *Documentation*: luuse.io/freefontlib
- *Featuring*: OpenTypeJs

---
title: Opbouw
date_pub: 2022/27/01, 14:39:19
font_family: Opbouw
project_url: https://www.instagram.com/edd.nz/
license: CC BY-ND
license_url: https://creativecommons.org/
designers:
 - Edouard Nazé
styles:
 - Regular
paths:
 - fonts/Opbouw/opbouw_regular.otf
tags:
 - Serif
ascender: 750
descender: -250
sample_text: "Monter sur son vélo et foncer à travers tout, sentir les bagnoles qui t’effleurent les côtes, que tu te faufiles, qu’il n’y a plus d’obstacles, plus de temps mort, tout est fluide, tu deviens le temps qui passe."
public: yes
---

 

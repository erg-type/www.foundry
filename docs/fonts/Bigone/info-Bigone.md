---
title: Bigone
date_pub: 2022/27/01, 16:00:37
font_family: Bigone
project_url: 
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Prudence Henken]
styles:
 - Regular
paths:
 - fonts/Bigone/Bigone.ttf
tags:
 - Sans Serif
ascender: 699
descender: -20
sample_text: "J’ai appris que les appareils photo russes sont plus solides que mes os en tombant de vélo.
"
public: yes
---

 

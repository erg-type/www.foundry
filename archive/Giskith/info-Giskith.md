---
title: Giskith
date_pub: 2022/25/01, 12:11:44
font_family: Giskith
project_url: 
license: 
license_url: 
designers:
 - [, ]
styles:
 - Regular
paths:
 - fonts/Giskith/Giskith.otf
tags:
ascender: 750
descender: -250
sample_text: "Récite moi l'alphabet."
public: no
---

 